﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    public class Padre
    {
        //el gion bajo representa una variable global
        private int _informacionPrivada;

        protected int _informacionProtegida { get; set; }

        //La primera letra de las propiedades va en mayusculas
        public string Propiedad1Padre { get; set; }

        public int Propiedad2Padre { get; set; }

        //El parametro de entrada sin guion bajo representa una variable local
        public void MetodoPadre()
        {
            Console.WriteLine("Metodo del padre");
        }
    }
}
