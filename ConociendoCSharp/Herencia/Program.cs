﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            //Cuando vean la palabra new se esta creando una instancia de una clase. 
            //una instancia es un objeto
            Hija hija = new Hija();
        }
    }
}
