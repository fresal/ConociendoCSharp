﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Figura> figuras = new List<Figura>();

            figuras.Add(new Figura());
            figuras.Add(new Linea());
            figuras.Add(new Circulo());

            foreach (Figura figura in figuras)
            {
                figura.Pintar();
            }

            Console.ReadLine();
        }
    }
}
