﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConociendoCSharp
{
    class Persona
    {
        public string Nombre { get; set; }

        public int Edad { get; set; }

        //public int Edad
        //{ get
        //    {
        //        //return DateTime.Now.Year - FechaNacimiento.Year;
        //        return DateTime.Today.AddTicks(-FechaNacimiento.Ticks).Year -1;
        //    }
        //}

        public DateTime FechaNacimiento { get; set; }

        public bool? Casado { get; set; }

        public Genero GeneroPersona { get; set; }

        public Persona()
        {

        }

        public Persona(string nombre, int edad)
        {
            Nombre = nombre;
            Edad = edad;
            FechaNacimiento = DateTime.Now;
        }

        public Persona(string nombre, int edad, int ano, int mes, int dia)
        {
            Nombre = nombre;
            Edad = edad;
            FechaNacimiento = new DateTime(ano, mes, dia);
        }

        public void MostrarInformacion()
        {
            Console.WriteLine("Nombre: {0} Edad {1}", Nombre, Edad);
        }

        public void MostrarTodaInformacion()
        {
            Console.WriteLine("{0} tiene {1} años, y {2}", Nombre, Edad, Casado.Value ? "está casado" : "no está casado");
        }

        public bool EsMayorEdad()
        {
            return Edad > 18;
        }
    }
}
