﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConociendoCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [Instanciando persona con constructor sin parametros]
            Persona persona1 = new Persona();
            persona1.Nombre = "Jairo";
            persona1.Edad = 37;
            persona1.GeneroPersona = Genero.Masculino;
            persona1.Casado = true;
            persona1.FechaNacimiento = new DateTime(1980, 8, 13);
            #endregion

            #region [Instanciando sin constructor y con propiedades entre llaves]
            Persona persona2 = new Persona
            {
                Nombre = "Lina",
                Edad = 38,
                FechaNacimiento = new DateTime(1979, 12, 29),
                Casado = true,
                GeneroPersona = Genero.Femenino
            };
            #endregion

            #region [ Mostrar información de persona 1 y 2 ]
            Console.WriteLine("nombre: " + persona1.Nombre + ", edad: " + persona1.Edad + ", fecha nacimiento: " + persona1.FechaNacimiento);
            Console.WriteLine("nombre: {0}, edad: {1}, fecha: {2}", persona2.Nombre, persona2.Edad, persona2.FechaNacimiento);
            #endregion

            Console.WriteLine("{0}{0}{0}", Environment.NewLine);

            #region [ Instanciando con el constructor de 2 parametros ]
            Persona persona3 = new Persona("Mariana", 12);
            persona3.GeneroPersona = Genero.Femenino;
            persona3.Casado = false;

            Console.WriteLine($"Nombre: {persona3.Nombre}, Edad: {persona3.Edad}, Fecha: {persona3.FechaNacimiento}");
            #endregion

            #region [ Instanciando con el constructor de 3 parametros ]
            Persona persona4 = new Persona("Juan Manuel", 7, 2011, 7, 24);
            persona4.GeneroPersona = Genero.Masculino;
            persona4.Casado = false;

            string information = string.Format("Nombre: {0}, Edad: {1}, Fecha: {2}", persona4.Nombre, persona4.Edad, persona4.FechaNacimiento.ToString("yyyy-MM-dd"));

            Console.WriteLine(information);
            #endregion

            Console.WriteLine("{0}{0}{0}", Environment.NewLine);

            #region [ Nullable ]
            int? valor = null;

            if (!valor.HasValue) // No tiene valor
            {
                valor = 10;
            }
            #endregion

            #region [ Invocando métodos ]
            persona1.MostrarInformacion();
            persona1.MostrarTodaInformacion();
            persona3.MostrarTodaInformacion();

            Console.WriteLine("{0}{0}{0}", Environment.NewLine);
            #endregion

            #region [ Invocando método que retorna un valor ]
            if (persona1.EsMayorEdad())
            {
                Console.WriteLine(persona1.Nombre + " es mayor de edad");
            }
            #endregion

            #region [ Generics ]
            List<int> numeroEnteros = new List<int>();

            numeroEnteros.Add(1);
            numeroEnteros.Add(2);

            foreach (int item in numeroEnteros)
            {
                Console.WriteLine(item);
            }

            List<string> listadoString = new List<string>();

            listadoString.Add("Jairo");
            listadoString.Add("Lina");
            listadoString.Add("Mariana");
            listadoString.Add("Juan");

            foreach (string item in listadoString)
            {
                Console.WriteLine(item);
            }

            List<string> otroListado = new List<string>()
            {
                "Pepito",
                "Juanita",
                "Manuel"
            };

            foreach (string item in otroListado)
            {
                Console.WriteLine(item);
            }
            #endregion

            Console.WriteLine("{0}{0}{0}", Environment.NewLine);

            List<Persona> personas = new List<Persona>()
            {
                persona1,
                persona2,
                persona3,
                persona4
            };

            personas.Add(new Persona { Nombre = "Raquel", Edad = 50, Casado = true, FechaNacimiento = DateTime.Now, GeneroPersona = Genero.Femenino });

            foreach (Persona persona in personas)
            {
                persona.MostrarTodaInformacion();
            }

            Console.ReadLine();
        }
}
}
