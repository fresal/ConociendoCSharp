﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionStringsMath
{
    class Program
    {
        static void Main(string[] args)
        {
            #region [ Convertir de string a un número]
            string strNumero1 = "123";
            //int numero1 = int.Parse(strNumero1);

            int numero1;
            bool esNumero = int.TryParse(strNumero1, out numero1);

            if (esNumero)
            {
                Console.WriteLine("Es Número: " + numero1);
            }
            else
            {
                Console.WriteLine("No es número");
            }

            //double.TryParse
            int numero2 = Convert.ToInt32(strNumero1);
            uint numero3 = Convert.ToUInt32(strNumero1);

            string convertirCadena = numero1.ToString();
            #endregion

            #region [ Strings o cadenas de caracteres]
            string nombre = "Frase de Prueba";

            //retorna la longitud de la cadena
            int longitud = nombre.Length;

            //si quiero saber si contiene una palabra o parte de una palabra
            bool siContiene = nombre.Contains("se");

            //si quiero saber la posicion del primer CARACTER que se busque
            int indice = nombre.IndexOf('a');

            //si quiero saber la posicion del ultimo CARACTER que se busque
            int indiceUltimo = nombre.LastIndexOf('a');

            //cortar parte de la cadena
            string corte = nombre.Remove(5);

            //remplazar un caracter por otro
            string nueva = nombre.Replace('a', '*');

            //quitar espacios
            string sinEspacios = nombre.Trim();

            //obtener un pedazo de la cadena
            string trozo = nombre.Substring(6, 2);
            #endregion

            #region [ Funciones Matematicas]
            //redondeo hacia arriba
            double resultadoArriba = Math.Ceiling(5.4);

            //redondeo hacia abajo
            double resultadoAbajo = Math.Floor(5.4);

            //Potencia
            double potencia = Math.Pow(2, 3);

            //Raiz cuadrada
            double raizCuadrada = Math.Sqrt(9);

            //Truncar
            double truncar = Math.Truncate(7.209);

            //redeondeo
            double redondear = Math.Round(7.209, 2);
            #endregion

            string ejercicio = "La controladora no encontro la ruta '/paginaX' o no ha sido implentada";

            string pagina = string.Empty;

            indice = ejercicio.IndexOf("'");
            indiceUltimo = ejercicio.LastIndexOf("'");

            pagina = ejercicio.Substring(indice + 2, indiceUltimo - indice - 2);

            Console.WriteLine(pagina);
            Console.ReadLine();
        }
    }
}
