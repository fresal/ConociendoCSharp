﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Calculadora
{
    class Calculadora
    {

        public static List<string> listaPares = new List<string>();
        public static List<string> listaImpares = new List<string>();
        public static List<string> listaMayoresCinco = new List<string>();

        public static int Sumar (int ope1, int ope2)
        {
            return ope1 + ope2;
        }

        public static int Restar(int ope1, int ope2)
        {
            return ope1 - ope2;
        }

        public static int Multiplicar(int ope1, int ope2)
        {
            return ope1 * ope2;
        }

        public static int Dividir(int ope1, int ope2)
        {
            return ope1 / ope2;
        }

        public static bool EsPar(int numero)
        {
            return numero % 2 == 0;
        }

        public static void ingresarNumeros()
        {
            string val;

            Console.WriteLine("Ingrese 6 números:");

            for (int i = 0; i < 6; i++)
            {
                Console.WriteLine("Ingrese número {0}", i+1);
                val = Console.ReadLine();

                if (EsPar(int.Parse(val)))
                {
                    listaPares.Add(val.ToString());   
                }
                else
                {
                    listaImpares.Add(val.ToString());
                }

                if (int.Parse(val) > 5)
                {
                    listaMayoresCinco.Add(val.ToString());
                }

            }
        }

        public static void imprimirNumeros()
        {
            Console.WriteLine("Números Pares:");
            foreach (string item in listaPares)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Números Impares:");
            foreach (string item in listaImpares)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Números Mayores que 5:");
            foreach (string item in listaMayoresCinco)
            {
                Console.WriteLine(item);
            }
        }
    }
}