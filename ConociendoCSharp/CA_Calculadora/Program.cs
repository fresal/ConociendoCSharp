﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Calculadora
{
    class Program
    {
        public enum Operacion { suma = '+', resta = '-', division = '/', multiplicacion = '*'};

        static void Main(string[] args)
        {
            int? val = null;
            Console.WriteLine("Digite Primer Número:");
            string ope1 = Console.ReadLine();
            Console.WriteLine("Digite Operación:");
            string operacion = Console.ReadLine();

            //Operacion operacion = (Operacion)Enum.Parse(typeof(Operacion), signo);

            Console.WriteLine("Digite Segundo Número:");
            string ope2 = Console.ReadLine();

            //if (operacion == Operacion.suma.ToString())
            //{
            //    val = Calculadora.Sumar(int.Parse(ope1), int.Parse(ope2));
            //} else if (operacion == Operacion.resta.ToString())
            //{
            //    val = Calculadora.Restar(int.Parse(ope1), int.Parse(ope2));
            //} else if (operacion == Operacion.multiplicacion.ToString())
            //{
            //    val = Calculadora.Multiplicar(int.Parse(ope1), int.Parse(ope2));
            //} else if (operacion == Operacion.multiplicacion.ToString())
            //{
            //    if (int.Parse(ope2) == 0)
            //    {
            //        Console.WriteLine("El segúndo número debe ser mayor a cero.");
            //    }
            //    else
            //    {
            //        val = Calculadora.Dividir(int.Parse(ope1), int.Parse(ope2));
            //    }
            //} else
            //{
            //    Console.WriteLine("La operación no es valida.");
            //}

            //string a = Operacion.suma.ToString();

            switch (operacion)
            {
                case "+":
                    val = Calculadora.Sumar(int.Parse(ope1), int.Parse(ope2));
                    break;
                case "-":
                    val = Calculadora.Restar(int.Parse(ope1), int.Parse(ope2));
                    break;
                case "*":
                    val = Calculadora.Multiplicar(int.Parse(ope1), int.Parse(ope2));
                    break;
                case "/":
                    if (int.Parse(ope2) == 0)
                    {
                        Console.WriteLine("El segúndo número debe ser mayor a cero.");
                    }
                    else
                    {
                        val = Calculadora.Dividir(int.Parse(ope1), int.Parse(ope2));
                    }
                    break;
                default:
                    Console.WriteLine("La operación no es valida.");
                    break;
            }

            if (val.HasValue)
            {
                Console.WriteLine("Resultado: {0}", val);
            }
            else
            {
                Console.WriteLine("No fue posible realizar la operación.");
            }

            Console.WriteLine();

            Calculadora.ingresarNumeros();

            Console.WriteLine();

            Calculadora.imprimirNumeros();

            Console.ReadLine();
        }
    }
}
