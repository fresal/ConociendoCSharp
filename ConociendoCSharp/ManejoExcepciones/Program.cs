﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManejoExcepciones
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //int i = 0;
                //int j = 10 / i;

                string dato = null;

                Funcion(dato);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }

        static void Funcion(string s)
        {
            if (s == null)
            {
                throw new Exception("El valor esta  nulo");
            }
        }
    }
}
